﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public float maxTapPressTime;
    private float beginTime, endTime;
    private MusicWindow currentSelectedWindow;

    public bool inputAllowed;

    private void Start()
    {
        currentSelectedWindow = null;
    }

    public void SetInputAllowed(bool allowed)
    {
        inputAllowed = allowed;
    }
    private void Update()
    {
        if (inputAllowed)
            HandleTouch();
    }
    void HandleTouch()
    {
            if (Input.GetMouseButtonDown(0))
            {
                MusicWindow aux;
                beginTime = Time.time;
                aux = SelectInteractableObject(Input.mousePosition);
                //Interact
                if (aux!=null)
                {
                    if (currentSelectedWindow != aux && currentSelectedWindow!=null)
                        currentSelectedWindow.OnInteractionEnded();
                    currentSelectedWindow = aux; 
                }
                else
                {
                    if (currentSelectedWindow != null)
                        currentSelectedWindow.OnInteractionEnded();
                    ExecutePressVoided();
                }
            }
            else if (Input.GetMouseButton(0))
            {
                MusicWindow aux;

                aux = SelectInteractableObject(Input.mousePosition);
            float time = Time.time;
                if(aux==null)
                {
                    if(currentSelectedWindow!=null)
                        currentSelectedWindow.OnInteractionEnded();
                    ExecutePressVoided();
                }
                else
                {
                    if (aux != currentSelectedWindow)
                    {
                        if (currentSelectedWindow != null)
                        {
                            currentSelectedWindow.OnInteractionEnded();
                        }
                    }
                    else
                    {
                        if(currentSelectedWindow!=null)
                        {
                            if (time - beginTime > maxTapPressTime)
                            {
                                currentSelectedWindow.HandleHold();
                            }
                        }
                    }
                }
                currentSelectedWindow = aux;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                endTime = Time.time;
                CalculateTapInterval(Input.mousePosition);
            }
    }

    private MusicWindow SelectInteractableObject(Vector3 position)
    {
        MusicWindow aux;
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(position),Vector2.zero);

        if (hit.collider != null)
        {
            aux = hit.collider.GetComponent<MusicWindow>();
            if (aux != null)
            {
                return aux;
            }
        }
        return null;
    }

    private void CalculateTapInterval(Vector3 position)
    {
        if (endTime - beginTime <= maxTapPressTime)
        {
            if(!SelectInteractableObject(position))
            {
                ExecutePressVoided();
            }
            else
            {
                ExecuteTap();
            }
        }
        else
        {
            if (!SelectInteractableObject(position))
            {
                ExecutePressVoided();
            }
            else
            {
                if (currentSelectedWindow != null)
                    currentSelectedWindow.OnInteractionEnded();
            }
        }
    }

    private void ExecuteTap()
    {
        currentSelectedWindow.OnTapExecuted();
    }

    private void ExecutePressVoided()
    {
        currentSelectedWindow = null;
    }
}