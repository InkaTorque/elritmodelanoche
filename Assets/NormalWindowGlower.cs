﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalWindowGlower : MonoBehaviour
{
    private Animator amimator;
    public float minWait, MaxWait;
    // Start is called before the first frame update
    void Start()
    {
        amimator = GetComponent<Animator>();
    }

    public void ActivateRandomWaitActivation()
    {
        float wait = UnityEngine.Random.Range(minWait, MaxWait);
        Timers.TimersManager.SetTimer(this, wait, ActivateGlow);
    }

    private void ActivateGlow()
    {
        amimator.SetTrigger("On");
    }
}
