﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomUtilities : MonoBehaviour
{
    public void DeactivateGameObject()
    {
        gameObject.SetActive(false);
    }

    public void StartClock()
    {
        AkSoundEngine.PostEvent("StartClock", gameObject);
    }
    public void StopClock()
    {
        AkSoundEngine.PostEvent("StopClock", gameObject);

    }
}
