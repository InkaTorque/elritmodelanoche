﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BubbleFiller : MonoBehaviour
{
    private Image fillBubble;
    public MusicWindow window;
    private void Start()
    {
        fillBubble = GetComponent<Image>();
    }

    private void Update()
    {
        if (window.holdActionAvailable)
        {
            fillBubble.fillAmount = Mathf.Clamp01(window.holdTimer / window.holdActivationTime);
        }
        else
            fillBubble.fillAmount = 0;
    }
}
