﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DefaultButton : MusicWindow
{
    public UnityEvent onTap;
    public override void OnTapExecuted()
    {
        onTap.Invoke();
    }

    public override void HandleHold()
    {
    }

    public override void OnInteractionEnded()
    {
    }
    private void Start()
    {
        
    }
}
