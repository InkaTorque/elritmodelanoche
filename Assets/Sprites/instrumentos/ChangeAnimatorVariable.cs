﻿using UnityEngine;

public enum AnimationEventType
{
    Enter,
    Exit,
    Update,
    EnterSubStateMachine,
    ExitSubStateMachine
}

public enum AnimationVariableType
{
    Bool,
    Int,
    Float,
    Trigger,
    Function
}

public enum AnimationOperationType
{
    Set,
    Add,
    Sub,
    Mult,
    Div
}

public enum AnimationOperationLocation
{
    Self,
    Parent,
    Child,
    AllChildren,
    AllChildrenOfChild
}

[SharedBetweenAnimators]
public class ChangeAnimatorVariable : StateMachineBehaviour
{
    [SerializeField] private AnimationEventType eventType;
    [SerializeField] private AnimationVariableType variableType;
    [SerializeField] private AnimationOperationType operationType;
    [SerializeField] private AnimationOperationLocation operationLocation;
    [SerializeField] private string variableName;
    [SerializeField] private bool newBoolValue;
    [SerializeField] private int newIntValue;
    [SerializeField] private float newFloatValue;
    [SerializeField] private bool resetTrigger;
    //[SerializeField] private bool changeOnParent;
    //[SerializeField] private bool changeOnChild;
    [SerializeField] private string childName;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (eventType == AnimationEventType.Enter)
            SetOperationLocation(animator);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (eventType == AnimationEventType.Update)
            SetOperationLocation(animator);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (eventType == AnimationEventType.Exit)
            SetOperationLocation(animator);
    }

    public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        base.OnStateMachineEnter(animator, stateMachinePathHash);

        if (eventType == AnimationEventType.EnterSubStateMachine)
            SetOperationLocation(animator);
    }

    public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    {
        base.OnStateMachineEnter(animator, stateMachinePathHash);

        if (eventType == AnimationEventType.ExitSubStateMachine)
            SetOperationLocation(animator);
    }

    void SetOperationLocation(Animator animator)
    {
        if (animator == null) return;

        switch (operationLocation)
        {
            case AnimationOperationLocation.Self:
                SetVariable(animator.transform);
                break;

            case AnimationOperationLocation.Parent:
                SetVariable(animator.transform.parent);
                break;

            case AnimationOperationLocation.Child:
                SetVariable(animator.transform.Find(childName));
                break;

            case AnimationOperationLocation.AllChildren:
                for (int i = 0; i < animator.transform.childCount; i++)
                    SetVariable(animator.transform.GetChild(i));
                break;

            case AnimationOperationLocation.AllChildrenOfChild:
                Transform child = animator.transform.Find(childName);
                for (int i = 0; i < child.childCount; i++)
                    SetVariable(child.GetChild(i));
                break;
        }

        /* if (operationLocation == AnimationOperationLocation.Child)
        {
            if (childName == "")
            {
                var animators = animator.GetComponentsInChildren<Animator>();

                for (int i = 0; i < animators.Length; i++)
                {
                    SetVariable(animators[i]);
                }
            }
            else
            {
                var animators = animator.transform.Find(childName).GetComponentsInChildren<Animator>();

                for (int i = 0; i < animators.Length; i++)
                {
                    SetVariable(animators[i]);
                }
            }
        }
        else if (operationLocation == AnimationOperationLocation.Parent)
        {
            SetVariable(animator.GetComponentInParent<Animator>());
        }
        else
        {
            SetVariable(animator);
        } */
    }

    private void SetVariable(Transform transform)
    {
        if (transform == null) return;
        if (variableType == AnimationVariableType.Function)
        {
            CallFunction(transform);
            return;
        }

        var animator = transform.GetComponent<Animator>();
        if (animator == null) return;

        switch (variableType)
        {
            case AnimationVariableType.Bool:
                SetBool(animator);
                break;
            case AnimationVariableType.Int:
                SetInteger(animator);
                break;
            case AnimationVariableType.Float:
                SetFloat(animator);
                break;
            case AnimationVariableType.Trigger:
                SetTrigger(animator);
                break;
        }
    }

    private void SetBool(Animator animator)
    {
        animator.SetBool(variableName, newBoolValue);
    }

    private void SetInteger(Animator animator)
    {
        switch (operationType)
        {
            case AnimationOperationType.Set:
                animator.SetInteger(variableName, newIntValue);
                break;
            case AnimationOperationType.Add:
                animator.SetInteger(variableName, animator.GetInteger(variableName) + newIntValue);
                break;
            case AnimationOperationType.Sub:
                animator.SetInteger(variableName, animator.GetInteger(variableName) - newIntValue);
                break;
            case AnimationOperationType.Mult:
                animator.SetInteger(variableName, animator.GetInteger(variableName) * newIntValue);
                break;
            case AnimationOperationType.Div:
                animator.SetInteger(variableName, animator.GetInteger(variableName) / newIntValue);
                break;
        }
    }

    private void SetFloat(Animator animator)
    {
        switch (operationType)
        {
            case AnimationOperationType.Set:
                animator.SetFloat(variableName, newFloatValue);
                break;
            case AnimationOperationType.Add:
                animator.SetFloat(variableName, animator.GetFloat(variableName) + newFloatValue);
                break;
            case AnimationOperationType.Sub:
                animator.SetFloat(variableName, animator.GetFloat(variableName) - newFloatValue);
                break;
            case AnimationOperationType.Mult:
                animator.SetFloat(variableName, animator.GetFloat(variableName) * newFloatValue);
                break;
            case AnimationOperationType.Div:
                animator.SetFloat(variableName, animator.GetFloat(variableName) / newFloatValue);
                break;
        }
    }

    private void SetTrigger(Animator animator)
    {
        if (resetTrigger)
            animator.ResetTrigger(variableName);
        else
            animator.SetTrigger(variableName);
    }

    private void CallFunction(Transform transform)
    {
        transform.SendMessage(variableName);
    }
}