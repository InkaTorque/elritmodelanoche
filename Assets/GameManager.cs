﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;
    public InputController ic;
    public bool start8PMOnStart;

    private List<RequiredInstruments> activatedList;
    public int maxInstruments;

    public GameObject title;
    public Animator animator;
    public GameObject[] windows;
    [Header("Wait Times")]
    public float fromPlayToZoomOutTime;
    public float clapGuyWaitTime,zoomInWaitTime, puzzleFinishZoomOutWaitTime, goToTitleWaitTime, goToMessageWaitTime;

    public GameObject clapGuy;

    public NormalWindowGlower[] normalWindows;

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(Instance);
            Instance = this;
        }
        activatedList = new List<RequiredInstruments>();
    }

    public void Start8PM()
    {
        Timers.TimersManager.SetTimer(this, fromPlayToZoomOutTime,WideShot);
    }

    private void WideShot()
    {
        animator.SetTrigger("WideShot");
    }

    public void OnWideShotFinished()
    {
        Timers.TimersManager.SetTimer(this, clapGuyWaitTime, ActivateClapGuy);
    }

    public void ActivateClapGuy()
    {
        clapGuy.GetComponent<MusicWindow>().ActivateVisualCue();
        activatedList.Add(RequiredInstruments.Claps);
        AkSoundEngine.PostEvent("Start8PM", gameObject);
        Timers.TimersManager.SetTimer(this, zoomInWaitTime, ZoomIn);
    }

    public void ZoomIn()
    {
        animator.SetTrigger("ZoomIn");
    }

    public void OnZoomInFinished()
    {
        ic.inputAllowed = true;
        ActivateOtherWindows();
    }

    private void ActivateOtherWindows()
    {
        for(int i = 0; i < windows.Length; i++)
        {
            windows[i].SetActive(true);
        }
    }

    public bool ActivationRequirementsMet(RequiredInstruments[] requirements)
    {
        bool completed=true;
        for(int i=0;i<requirements.Length;i++)
        {
            if(!activatedList.Contains(requirements[i]))
            {
                completed = false;
                break;
            }
        }
        return completed;
    }

    internal void OnTrackActivated(RequiredInstruments windowInstrumentType)
    {
        activatedList.Add(windowInstrumentType);
        if(maxInstruments+1==activatedList.Count)
        {
            OnPuzzleFinished();
        }
    }

    private void OnPuzzleFinished()
    {
        ic.inputAllowed = false;
        Timers.TimersManager.SetTimer(this, puzzleFinishZoomOutWaitTime, ZoomOut);
    }

    private void ZoomOut()
    {
       // AkSoundEngine.PostEvent("StartCelebration", gameObject);
        animator.SetTrigger("ZoomOut");
    }

   public void OnZoomOutFinished()
    {
        AkSoundEngine.PostEvent("PlayCrowNoises", gameObject);
        ActivateRandomGlows();
        Timers.TimersManager.SetTimer(this, goToTitleWaitTime, GoToTitle);
    }

    private void ActivateRandomGlows()
    {
        for(int i =0;i<normalWindows.Length;i++)
        {
            normalWindows[i].ActivateRandomWaitActivation();
        }
    }

    private void GoToTitle()
    {
        animator.SetTrigger("ToTitle");
    }

    public void OnGoTitleFinished()
    {
        Timers.TimersManager.SetTimer(this, goToMessageWaitTime, GoToMessage);
    }

    private void GoToMessage()
    {
       // AkSoundEngine.PostEvent("StartGrandFinale", gameObject);
        animator.SetTrigger("ToMessage");
    }

    public void OnGoToMessageFinished()
    {

    }
}
