﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class PlayButton : MusicWindow
{
    Animator animator;
    public Animator clockAnimator,infoAnimator;
    private void Start()
    {
        animator = GetComponent<Animator>();
        GameManager.Instance.ic.inputAllowed = true;
    }
    public override void HandleHold()
    {
    }
    public override void OnInteractionEnded()
    {

    }

    public override void OnTapExecuted()
    {
        GameManager.Instance.ic.inputAllowed = false;
        GetComponent<BoxCollider2D>().enabled = false;
        animator.SetTrigger("Vanish");
        infoAnimator.SetTrigger("Vanish");
        Timers.TimersManager.SetTimer(this, 2, VanishClock);
        GameManager.Instance.Start8PM();
    }

    public void VanishClock()
    {
        clockAnimator.SetTrigger("Vanish");

    }
}
