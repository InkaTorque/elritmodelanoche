﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public enum RequiredInstruments{
    Instrument1=0,
    Instrument2=1,
    Instrument3=2,
    Instrument4=3,
    Instrument5=4,
    Instrument6=5,
    Claps=6
}
public class MusicWindow : MonoBehaviour
{
    public RequiredInstruments windowInstrumentType;
    public GameObject canvas, particlces, icon;
    public RequiredInstruments[] requiredInstruments;
    public bool holdActionAvailable,onHold;
    public float holdActivationTime;
    public float holdTimer;
    private BoxCollider2D collider;
    public Sprite activeSprite;
    private void Start()
    {
        holdActionAvailable = false;
        holdTimer=0;
        collider = GetComponent<BoxCollider2D>();
    }

    public virtual void HandleHold()
    {
        if(holdActionAvailable)
        {
            if (GameManager.Instance.ActivationRequirementsMet(requiredInstruments))
            {
                holdTimer += Time.deltaTime;
                if (holdTimer >= holdActivationTime)
                {
                    OnHoldCompleted();
                }
            }
        }
    }

    private void OnHoldCompleted()
    {
        ActivateTrack();
        ActivateVisualCue();
    }

    public void ActivateVisualCue()
    {
       // icon.gameObject.SetActive(true);
        particlces.gameObject.SetActive(true);
        canvas.gameObject.SetActive(false);
        collider.enabled = false;
        GetComponent<SpriteRenderer>().sprite = activeSprite;
    }

    public virtual void OnInteractionEnded()
    {
        canvas.gameObject.SetActive(false);
    }

    public virtual void OnTapExecuted()
    {
        if(canvas.gameObject.activeSelf)
        {
            canvas.gameObject.SetActive(false);
        }
        else
        {
            canvas.gameObject.SetActive(true);
        }
        holdActionAvailable = canvas.activeInHierarchy;
        holdTimer = 0.0f;
    }

    public void ActivateTrack()
    {
        AkSoundEngine.SetSwitch(windowInstrumentType.ToString(), "On", Camera.main.gameObject);
        GameManager.Instance.OnTrackActivated(windowInstrumentType);
    }
}
